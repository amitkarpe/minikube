debian:
	sudo apt-get remove docker docker-engine docker.io containerd runc -y | true
	sudo apt-get update
	sudo apt-get install apt-transport-https ca-certificates curl gnupg software-properties-common -y
	curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
	sudo apt-key fingerprint 0EBFCD88
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
	# Following line is not working.
	#sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" 
	sudo apt-get update -y
	sudo apt-get install docker-ce docker-ce-cli containerd.io -y
#	sudo apt-get install kubelet kubectl kubeadm
#	sudo apt-get update && sudo apt-get install -y apt-transport-https
#	curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#	echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
	

minikube:
	curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
	sudo mkdir -p /usr/local/bin/
	sudo install minikube /usr/local/bin/

start:
	sudo minikube start --vm-driver=none
	#minikube start --vm-driver=kvm2
